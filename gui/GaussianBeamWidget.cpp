/* This file is part of the GaussianBeam project
   Copyright (C) 2007-2010 Jérôme Lodewyck <jerome dot lodewyck at normalesup.org>
   Copyright (C) 2024 Balázs Dura-Kovács

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "src/GaussianBeam.h"
#include "src/OpticsBench.h"
#include "src/GaussianFit.h"

#include "gui/GaussianBeamWidget.h"
#include "gui/GaussianBeamWindow.h"
#include "gui/OpticsView.h"
#include "gui/Unit.h"
#ifdef GBPLOT
	#include "gui/GaussianBeamPlot.h"
#endif

#include "./ui_GaussianBeamWidget.h"

#include <QApplication>
#include <QPushButton>
#include <QStandardItemModel>
#include <QDebug>
#include <QMessageBox>
#include <QMenu>
#include <QColorDialog>
#include <QInputDialog>
#include <QSettings>

#include <cmath>

GaussianBeamWidget::GaussianBeamWidget(OpticsBench* bench, GaussianBeamWindow* window)
	: QWidget(window)
	, m_window(window)
	, ui(new Ui::GaussianBeamWidget)
{
	m_updatingFit = false;
	m_updatingTarget = false;
	ui->setupUi(this);

	m_bench = bench;
	m_bench->registerEventListener(this);

	//toolBox->setSizeHint(100);
	//toolBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored));

	// Extra widgets, not included in designer
#ifdef GBPLOT
/*	plot = new GaussianBeamPlot(this, model);
	splitter->addWidget(plot);
	checkBox_ShowGraph->setVisible(true);
	checkBox_ShowGraph->setEnabled(true);
	//checkBox_ShowGraph->setChecked(true);
	plot->setVisible(checkBox_ShowGraph->isChecked());*/
#else
/*	checkBox_ShowGraph->setVisible(false);
	checkBox_ShowGraph->setEnabled(false);*/
#endif

	// Waist fit
	fitModel = new QStandardItemModel(0, 3, this);
	ui->fitTable->setModel(fitModel);
	ui->fitTable->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->fitTable->setSelectionMode(QAbstractItemView::ExtendedSelection);
	fitSelectionModel = new QItemSelectionModel(fitModel);
	ui->fitTable->setSelectionModel(fitSelectionModel);
	ui->fitTable->setColumnWidth(0, 82);
	ui->fitTable->setColumnWidth(1, 82);

	// Connect slots	
	QObject::connect(fitModel, &QStandardItemModel::dataChanged, this, &GaussianBeamWidget::fitModelChanged);

	// Set up default values
	on_checkBox_ShowTargetBeam_toggled(ui->checkBox_ShowTargetBeam->isChecked());
	updateUnits();
	readSettings();

	// Sync with bench
	onOpticsBenchTargetBeamChanged();
	onOpticsBenchWavelengthChanged();
	onOpticsBenchBoundariesChanged();
	for (int i = 0; i < m_bench->nFit(); i++)
		onOpticsBenchFitAdded(i);

	updateFitInformation(ui->comboBox_Fit->currentIndex());
	updateTargetInformation();
}

GaussianBeamWidget::~GaussianBeamWidget()
{
	writeSettings();
	delete ui;
}

void GaussianBeamWidget::writeSettings()
{
	QSettings settings;
	settings.setValue("GaussianBeamWidget/toolboxIndex", ui->toolBox->currentIndex());
}

void GaussianBeamWidget::readSettings()
{
	QSettings settings;
	ui->toolBox->setCurrentIndex(settings.value("GaussianBeamWidget/toolboxIndex", 0).toInt());
}

void GaussianBeamWidget::updateUnits()
{
	ui->doubleSpinBox_HTargetWaist->setSuffix(Unit(UnitWaist).string());
	ui->doubleSpinBox_HTargetPosition->setSuffix(Unit(UnitPosition).string());
	ui->doubleSpinBox_VTargetWaist->setSuffix(Unit(UnitWaist).string());
	ui->doubleSpinBox_VTargetPosition->setSuffix(Unit(UnitPosition).string());
	ui->doubleSpinBox_LeftBoundary->setSuffix(Unit(UnitPosition).string());
	ui->doubleSpinBox_RightBoundary->setSuffix(Unit(UnitPosition).string());
	/// @todo update table headers and status bar and wavelength
}

void GaussianBeamWidget::onOpticsBenchDataChanged(int /*startOptics*/, int /*endOptics*/)
{
	displayOverlap();
}

void GaussianBeamWidget::onOpticsBenchWavelengthChanged()
{
	displayOverlap();
	updateFitInformation(ui->comboBox_Fit->currentIndex());
}

///////////////////////////////////////////////////////////
// OPTICS BENCH PAGE

void GaussianBeamWidget::on_doubleSpinBox_LeftBoundary_valueChanged(double value)
{
	m_bench->setLeftBoundary(value*Unit(UnitPosition).multiplier());
}

void GaussianBeamWidget::on_doubleSpinBox_RightBoundary_valueChanged(double value)
{
	m_bench->setRightBoundary(value*Unit(UnitPosition).multiplier());
}

void GaussianBeamWidget::onOpticsBenchBoundariesChanged()
{
	ui->doubleSpinBox_LeftBoundary->setValue(m_bench->leftBoundary()*Unit(UnitPosition).divider());
	ui->doubleSpinBox_RightBoundary->setValue(m_bench->rightBoundary()*Unit(UnitPosition).divider());
}

///////////////////////////////////////////////////////////
// CAVITY PAGE

void GaussianBeamWidget::updateCavityInformation()
{

}

///////////////////////////////////////////////////////////
// MAGIC WAIST PAGE

void GaussianBeamWidget::displayOverlap()
{
	if (m_bench->nOptics() > 0)
	{
		double overlap = Beam::overlap(*m_bench->beam(m_bench->nOptics()-1), *m_bench->targetBeam());
		ui->label_OverlapResult->setText(tr("Overlap: ") + QString::number(overlap*100., 'f', 2) + " %");
	}
	else
		ui->label_OverlapResult->setText("");
}

void GaussianBeamWidget::updateTargetInformation()
{
	m_updatingTarget = true;

	if (m_bench->targetOrientation() == Ellipsoidal)
	{
		ui->comboBox_TargetOrientation->setCurrentIndex(1);
		ui->doubleSpinBox_VTargetWaist->setVisible(true);
		ui->doubleSpinBox_VTargetPosition->setVisible(true);
	}
	else
	{
		ui->comboBox_TargetOrientation->setCurrentIndex(0);
		ui->doubleSpinBox_VTargetWaist->setVisible(false);
		ui->doubleSpinBox_VTargetPosition->setVisible(false);
	}

	ui->doubleSpinBox_HTargetWaist->setValue(m_bench->targetBeam()->waist(Horizontal)*Unit(UnitWaist).divider());
	ui->doubleSpinBox_HTargetPosition->setValue(m_bench->targetBeam()->waistPosition(Horizontal)*Unit(UnitPosition).divider());
	ui->doubleSpinBox_VTargetWaist->setValue(m_bench->targetBeam()->waist(Vertical)*Unit(UnitWaist).divider());
	ui->doubleSpinBox_VTargetPosition->setValue(m_bench->targetBeam()->waistPosition(Vertical)*Unit(UnitPosition).divider());
	ui->doubleSpinBox_MinOverlap->setValue(m_bench->targetOverlap()*100.);
	displayOverlap();

	m_updatingTarget = false;
}

void GaussianBeamWidget::on_comboBox_TargetOrientation_currentIndexChanged(int dataIndex)
{
	if (m_updatingTarget)
		return;

	if (dataIndex == 0)
		m_bench->setTargetOrientation(Spherical);
	else
		m_bench->setTargetOrientation(Ellipsoidal);
}

void GaussianBeamWidget::on_doubleSpinBox_HTargetWaist_valueChanged(double value)
{
	if (m_updatingTarget)
		return;

	Orientation orientation = Horizontal;
	if (m_bench->targetOrientation() == Spherical)
		orientation = Spherical;

	Beam beam = *m_bench->targetBeam();
	beam.setWaist(value*Unit(UnitWaist).multiplier(), orientation);
	m_bench->setTargetBeam(beam);
}

void GaussianBeamWidget::on_doubleSpinBox_HTargetPosition_valueChanged(double value)
{
	if (m_updatingTarget)
		return;

	Orientation orientation = Horizontal;
	if (m_bench->targetOrientation() == Spherical)
		orientation = Spherical;

	Beam beam = *m_bench->targetBeam();
	beam.setWaistPosition(value*Unit(UnitPosition).multiplier(), orientation);
	m_bench->setTargetBeam(beam);
}

void GaussianBeamWidget::on_doubleSpinBox_VTargetWaist_valueChanged(double value)
{
	if (m_updatingTarget)
		return;

	Beam beam = *m_bench->targetBeam();
	beam.setWaist(value*Unit(UnitWaist).multiplier(), Vertical);
	m_bench->setTargetBeam(beam);
}

void GaussianBeamWidget::on_doubleSpinBox_VTargetPosition_valueChanged(double value)
{
	if (m_updatingTarget)
		return;

	Beam beam = *m_bench->targetBeam();
	beam.setWaistPosition(value*Unit(UnitPosition).multiplier(), Vertical);
	m_bench->setTargetBeam(beam);
}

void GaussianBeamWidget::on_doubleSpinBox_MinOverlap_valueChanged(double value)
{
	if (m_updatingTarget)
		return;

	m_bench->setTargetOverlap(value*0.01);
}

void GaussianBeamWidget::on_checkBox_ShowTargetBeam_toggled(bool checked)
{
	if (m_updatingTarget)
		return;

	m_window->showTargetBeam(checked);
}

void GaussianBeamWidget::on_pushButton_MagicWaist_clicked()
{
	ui->label_MagicWaistResult->setText("");

	if (!m_bench->magicWaist())
		ui->label_MagicWaistResult->setText(tr("Desired waist could not be found !"));
	else
		displayOverlap();
}

void GaussianBeamWidget::on_pushButton_LocalOptimum_clicked()
{
	if (!m_bench->localOptimum())
		ui->label_MagicWaistResult->setText(tr("Local optimum not found !"));
	else
		displayOverlap();
}

void GaussianBeamWidget::onOpticsBenchTargetBeamChanged()
{
	updateTargetInformation();
}

///////////////////////////////////////////////////////////
// FIT PAGE

QVariant GaussianBeamWidget::formattedFitData(double value)
{
	if (value != 0.)
		return value*Unit(UnitWaist).divider();

	return QString("");
}

void GaussianBeamWidget::updateFitInformation(int index)
{
	if (index >= m_bench->nFit())
		return;

	// Enable or disable widgets
	if (index < 0)
	{
		ui->pushButton_RemoveFit->setEnabled(false);
		ui->pushButton_RenameFit->setEnabled(false);
		ui->pushButton_FitColor->setEnabled(false);
		ui->pushButton_FitAddRow->setEnabled(false);
		ui->pushButton_FitRemoveRow->setEnabled(false);
		ui->comboBox_FitData->setEnabled(false);
		ui->comboBox_FitOrientation->setEnabled(false);
		ui->fitTable->setEnabled(false);

		ui->pushButton_SetInputBeam->setEnabled(false);
		ui->pushButton_SetTargetBeam->setEnabled(false);
		return;
	}
	else
	{
		ui->pushButton_RemoveFit->setEnabled(true);
		ui->pushButton_RenameFit->setEnabled(true);
		ui->pushButton_FitColor->setEnabled(true);
		ui->pushButton_FitAddRow->setEnabled(true);
		ui->pushButton_FitRemoveRow->setEnabled(true);
		ui->comboBox_FitData->setEnabled(true);
		ui->comboBox_FitOrientation->setEnabled(true);
		ui->fitTable->setEnabled(true);
	}

	m_updatingFit = true;
	// Fill widgets
	Fit* fit = m_bench->fit(index);
	ui->comboBox_Fit->setItemText(index, QString::fromUtf8(fit->name().c_str()));
	ui->pushButton_FitColor->setPalette(QPalette(QColor(fit->color())));
	ui->comboBox_FitData->setCurrentIndex(int(fit->dataType()));
	ui->comboBox_FitOrientation->setCurrentIndex(int(fit->orientation()));

	// Resize rows to match the number of elements in the fit
	while (fit->size() > fitModel->rowCount())
		fitModel->insertRow(0);
	while (fitModel->rowCount() > fit->size())
		fitModel->removeRow(0);

	// Show/hide fit columns
	ui->fitTable->showColumn(1);
	ui->fitTable->showColumn(2);
	if ((fit->orientation() == Horizontal) || (fit->orientation() == Spherical))
		ui->fitTable->hideColumn(2);
	else if (fit->orientation() == Vertical)
		ui->fitTable->hideColumn(1);

	QString header = (fit->orientation() == Spherical) ? tr("Value") : tr("Horizontal\nvalue");
	fitModel->setHeaderData(0, Qt::Horizontal, tr("Position") + "\n(" + Unit(UnitPosition).string(false) + ")");
	fitModel->setHeaderData(1, Qt::Horizontal, header + "\n(" + Unit(UnitWaist).string(false) + ")");
	fitModel->setHeaderData(2, Qt::Horizontal, tr("Vertical\nvalue") + "\n(" + Unit(UnitWaist).string(false) + ")");

	// Fill the fit table
	for (int i = 0; i < fit->size(); i++)
	{
		if ((fit->position(i) != 0.) || fit->nonZeroEntry(i))
			fitModel->setData(fitModel->index(i, 0), fit->position(i)*Unit(UnitPosition).divider());
		else
			fitModel->setData(fitModel->index(i, 0), QString(""));

		if (fit->orientation() != Vertical)
			fitModel->setData(fitModel->index(i, 1), formattedFitData(fit->value(i, Horizontal)));
		if (fit->orientation() != Horizontal)
			fitModel->setData(fitModel->index(i, 2), formattedFitData(fit->value(i, Vertical)));
	}

	bool fh = fit->fitAvailable(Horizontal);
	bool fv = (fit->fitAvailable(Vertical) & (fit->orientation() != Spherical));
	if (fh | fv)
	{
		Beam fitBeam(m_bench->wavelength());
		double residue = fit->applyFit(fitBeam);
		QString text;
		if (fh)
		{
			text += (fv ? tr("Horizonal waist") : tr("Waist")) + " = " + QString::number(fitBeam.waist(Horizontal)*Unit(UnitWaist).divider()) + Unit(UnitWaist).string() + "\n" +
					(fv ? tr("Horizonal position") : tr("Position")) + " = " + QString::number(fitBeam.waistPosition(Horizontal)*Unit(UnitPosition).divider()) + Unit(UnitPosition).string() + "\n";
		}
		if (fv)
		{
			text += (fh ? tr("Vertical waist") : tr("Waist")) + " = " + QString::number(fitBeam.waist(Vertical)*Unit(UnitWaist).divider()) + Unit(UnitWaist).string() + "\n" +
					(fh ? tr("Vertical position") : tr("Position")) + " = " + QString::number(fitBeam.waistPosition(Vertical)*Unit(UnitPosition).divider()) + Unit(UnitPosition).string() + "\n";
		}
		text += tr("Residue") + " = " + QString::number(residue);
		ui->label_FitResult->setText(text);
		ui->pushButton_SetInputBeam->setEnabled(true);
		ui->pushButton_SetTargetBeam->setEnabled(true);
	}
	else
	{
		ui->pushButton_SetInputBeam->setEnabled(false);
		ui->pushButton_SetTargetBeam->setEnabled(false);
		ui->label_FitResult->setText(QString());
	}

	m_updatingFit = false;
}

// Control callback

void GaussianBeamWidget::on_comboBox_Fit_currentIndexChanged(int index)
{
	updateFitInformation(index);
}

void GaussianBeamWidget::on_pushButton_AddFit_clicked()
{
	int index = m_bench->nFit();
	m_bench->addFit(index, 3);
}

void GaussianBeamWidget::on_pushButton_RemoveFit_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	m_bench->removeFit(index);
}

void GaussianBeamWidget::on_pushButton_RenameFit_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	bool ok;
	QString text = QInputDialog::getText(this, tr("Rename fit"),
		tr("Enter a new name for the current fit:"), QLineEdit::Normal,
		m_bench->fit(index)->name().c_str(), &ok);
	if (ok && !text.isEmpty())
		m_bench->fit(index)->setName(text.toUtf8().data());
}

void GaussianBeamWidget::on_pushButton_FitColor_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	QColor color = QColorDialog::getColor(Qt::black, this);
	m_bench->fit(index)->setColor(color.rgb());
}

void GaussianBeamWidget::on_comboBox_FitOrientation_currentIndexChanged(int dataIndex)
{
	int index = ui->comboBox_Fit->currentIndex();
	if ((index < 0) || m_updatingFit)
		return;

	Fit* fit = m_bench->fit(index);
	fit->setOrientation(Orientation(dataIndex));
}

void GaussianBeamWidget::on_comboBox_FitData_currentIndexChanged(int dataIndex)
{
	int index = ui->comboBox_Fit->currentIndex();
	if ((index < 0) || m_updatingFit)
		return;

	Fit* fit = m_bench->fit(index);
	fit->setDataType(FitDataType(dataIndex));
}

void GaussianBeamWidget::fitModelChanged(const QModelIndex& start, const QModelIndex& stop)
{
	int index = ui->comboBox_Fit->currentIndex();
	if ((index < 0) || m_updatingFit)
		return;

	Fit* fit = m_bench->fit(index);

	for (int row = start.row(); row <= stop.row(); row++)
	{
		double position = fitModel->data(fitModel->index(row, 0)).toDouble()*Unit(UnitPosition).multiplier();
		double hValue = fitModel->data(fitModel->index(row, 1)).toDouble()*Unit(UnitWaist).multiplier();
		double vValue = fitModel->data(fitModel->index(row, 2)).toDouble()*Unit(UnitWaist).multiplier();
		if (fit->orientation() == Spherical)
			fit->setData(row, position, hValue, Spherical);
		else
		{
			if (fit->orientation() != Vertical)
				fit->setData(row, position, hValue, Horizontal);
			if (fit->orientation() != Horizontal)
				fit->setData(row, position, vValue, Vertical);
		}
	}
}

void GaussianBeamWidget::on_pushButton_FitAddRow_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	m_bench->fit(index)->addData(0., 0., Spherical);
}

void GaussianBeamWidget::on_pushButton_FitRemoveRow_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	QList<int> removedRows;

	for (int row = fitModel->rowCount() - 1; row >= 0; row--)
		if (fitSelectionModel->isRowSelected(row, QModelIndex()) && (row < m_bench->fit(index)->size()))
			removedRows << row;

	foreach (int row, removedRows)
		m_bench->fit(index)->removeData(row);
}

void GaussianBeamWidget::on_pushButton_SetInputBeam_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	Beam inputBeam = *m_bench->inputBeam();
	m_bench->fit(index)->applyFit(inputBeam);
	m_bench->setInputBeam(inputBeam);
}

void GaussianBeamWidget::on_pushButton_SetTargetBeam_clicked()
{
	int index = ui->comboBox_Fit->currentIndex();
	if (index < 0)
		return;

	Beam targetBeam = *m_bench->targetBeam();
	m_bench->fit(index)->applyFit(targetBeam);
	m_bench->setTargetBeam(targetBeam);
}

void GaussianBeamWidget::displayShowTargetBeam(bool show)
{
	ui->checkBox_ShowTargetBeam->setCheckState(show ? Qt::Checked : Qt::Unchecked);
}

// OpticsBench callbacks

void GaussianBeamWidget::onOpticsBenchFitAdded(int index)
{
	ui->comboBox_Fit->insertItem(index, QString::fromUtf8(m_bench->fit(index)->name().c_str()));
	ui->comboBox_Fit->setCurrentIndex(index);
}

void GaussianBeamWidget::onOpticsBenchFitsRemoved(int index, int count)
{
	for (int i = index + count - 1; i >= index; i--)
		ui->comboBox_Fit->removeItem(i);
}

void GaussianBeamWidget::onOpticsBenchFitDataChanged(int index)
{
	int comboIndex = ui->comboBox_Fit->currentIndex();
	if ((comboIndex < 0) || (comboIndex != index))
		return;

	updateFitInformation(index);
}
