/*
 * This file is part of the GaussianBeam project
 * Copyright (C) 2006  Christophe Dumez <chris@qbittorrent.org>
 * Copyright (C) 2024  Balázs Dura-Kovács
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include <QDialogButtonBox>
#include <QFile>

AboutDialog::AboutDialog(QWidget *parent) :
	QDialog(parent, Qt::Dialog | Qt::WindowCloseButtonHint),
	ui(new Ui::AboutDialog)
{
	ui->setupUi(this);

	// Title
	ui->labelName->setText(QStringLiteral("<b><h2>%1 %2 (%3-bit)</h2></b>").arg(QApplication::applicationName(), QApplication::applicationVersion(), QString::number(QT_POINTER_SIZE * 8)));

	// About
	const QString aboutText = QString{
		"<p>GaussianBeam is a Gaussian optics simulator.</p>"
		"<p>Copyright © 2007-2010 Jérôme Lodewyck<br>Copyright ©  2024 Balázs Dura-Kovács</p>"
		"<p>Images from KDE's Oxygen style</p>"
		"<p>Features:</p>"
		"<ul>"
			"<li>Table top display of the optical setup.</li>"
			"<li>Computes the beam parameters.</li>"
			"<li>Move optics with the mouse.</li>"
			"<li>Available optics: lenses, flat and curved interfaces, mirrors, dielectric slab and generic ABCD.</li>"
			"<li>Magic waist function: given a target waist, GaussianBeam finds the suitable arrangement of lenses.</li>"
			"<li>Fit waist function: measure the beam radius or diameter to define the input beam.</li>"
			"<li>Save and load configurations.</li>"
		"</ul><p></p>"
		"<table>"
		"<tr><td>%1</td><td><a href=\"%2\">%2</a></td></tr>"
		"<tr><td>%3</td><td><a href=\"%4\">%4</a></td></tr>"
		"</table>"
		}
		.arg(
			tr("Home Page:"),
			"https://gitlab.com/balping/gaussian-beam/",
			tr("Bug Tracker:"),
			"https://gitlab.com/balping/gaussian-beam/-/issues"
		 );
	ui->labelAbout->setText(aboutText);

	QDialogButtonBox * buttonBox = new QDialogButtonBox(QDialogButtonBox::Close, this);
	connect(buttonBox, &QDialogButtonBox::clicked, this, &QDialog::accept);
	ui->aboutDlgLayout->addWidget(buttonBox);

	// License
	QFile license_resource(":/COPYING");
	license_resource.open(QIODevice::ReadOnly);
	ui->textBrowserLicense->setPlainText(license_resource.readAll());

	// Software Used
	ui->labelQtVer->setText(QStringLiteral(QT_VERSION_STR));
	ui->labelLmfitVer->setText("3.2");
}

AboutDialog::~AboutDialog()
{
	delete ui;
}
