# Gaussian Beam

Gaussian Beam is a Gaussian optics simulator

## About the fork

This is a port to Qt6 of the [original package](https://sourceforge.net/projects/gaussianbeam/) written by Jérôme Lodewyck.

## Features

* Table top display of the optical setup.
* Computes the beam parameters.
* Move optics with the mouse.
* Available optics: lenses, flat and curved interfaces, mirrors, dielectric slab and generic ABCD.
* Magic waist function: given a target waist, GaussianBeam finds the suitable arrangement of lenses.
* Fit waist function: measure the beam radius or diameter to define the input beam.
* Save and load configurations.


## License

GPL v2